# eye-tracking-data-AVI24-article

**This repository contains anonymized eye-tracking data associated with the article "How complexity influences the use of a self-tracking tool?" (2024) submitted to the 17th International Conference on Advanced Visual Interfaces.**

## USAGE

- All data files are in CSV format.

- Data files adopt the following naming convention:
&lt;PXX&gt;_ &lt;G&gt;_ &lt;A&gt;_ &lt;T&gt;.csv

 - &lt;PXX&gt;: Participant number
 - &lt;G&gt;: Participant gender
 - &lt;A&gt;: Particiapnt age
 - &lt;T&gt;: Type of data (allgaze or fixations)


In directory **Eye-tracking data**, you will find participant data organized as follows:

- &lt;PXX&gt;_ &lt;G&gt;_ &lt;A&gt;_allgaze.csv: **This type of file contains all the data collected on a specific participant.**
- &lt;PXX&gt;_ &lt;G&gt;_ &lt;A&gt;_fixations.csv: **This type of file contains data collected on the variables associated with a specific participant's fixations.**


## MATCHING COLUMNS IN DATA

These images, taken from Gazepoint's official documentation, provide a description of each column, which can be found in the CSV files.

![First part of the official documentation](/images/1stpart_doc.png)
![Second part of the official documentation](/images/2ndpart_doc.png)
![Third part of the official documentation](/images/3rdpart_doc.png)

## LICENSE
This work is licensed under CC BY-NC-ND 4.0 